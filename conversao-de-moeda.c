#include <stdio.h>
#include <stdlib.h>

int main() {
    int nHidrogenios, nGravidades, nGases, nHelios;
    int moedaTerra = 0;
    int salvaProximoValor = 0;
    char leTipo[3];

    printf("Escreva o valor inteiro a ser convertido em Jupiter:\n");
    scanf("%s", leTipo);
    scanf("%d", &moedaTerra);

    nGases = (moedaTerra / 50);
    salvaProximoValor = (moedaTerra % 50);

    nGravidades = (salvaProximoValor / 10);
    salvaProximoValor = (salvaProximoValor % 10);

    nHelios = (salvaProximoValor / 5);
    salvaProximoValor = (salvaProximoValor % 5);

    nHidrogenios = salvaProximoValor;

    printf("%d hidrogenio\n", nHidrogenios);
    printf("%d helio\n", nHelios);
    printf("%d gravidade\n", nGravidades);
    printf("%d gas\n", nGases);

    return 0;
}